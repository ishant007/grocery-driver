package com.onedaycart.driver.retrofitModels.orderlistresponse;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("order_accepted_status")
	private String orderAcceptedStatus;

	@SerializedName("address")
	private String address;

	@SerializedName("partial_order_id")
	private String partialOrderId;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("status")
	private String status;

	public void setOrderAcceptedStatus(String orderAcceptedStatus){
		this.orderAcceptedStatus = orderAcceptedStatus;
	}

	public String getOrderAcceptedStatus(){
		return orderAcceptedStatus;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setPartialOrderId(String partialOrderId){
		this.partialOrderId = partialOrderId;
	}

	public String getPartialOrderId(){
		return partialOrderId;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"order_accepted_status = '" + orderAcceptedStatus + '\'' + 
			",address = '" + address + '\'' + 
			",partial_order_id = '" + partialOrderId + '\'' + 
			",order_id = '" + orderId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}