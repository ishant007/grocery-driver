package com.onedaycart.driver.retrofitModels.otpres;

import com.google.gson.annotations.SerializedName;

public class OtpResponse{

	@SerializedName("otp")
	private int otp;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setOtp(int otp){
		this.otp = otp;
	}

	public int getOtp(){
		return otp;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OtpResponse{" + 
			"otp = '" + otp + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}