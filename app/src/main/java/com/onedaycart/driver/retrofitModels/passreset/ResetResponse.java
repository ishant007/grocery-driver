package com.onedaycart.driver.retrofitModels.passreset;

import com.google.gson.annotations.SerializedName;

public class ResetResponse{

	@SerializedName("error")
	private String error;

	@SerializedName("status")
	private int status;

	public void setError(String error){
		this.error = error;
	}

	public String getError(){
		return error;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResetResponse{" + 
			"error = '" + error + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}