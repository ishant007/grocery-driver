package com.onedaycart.driver.retrofitModels.acceptOrder;

import com.google.gson.annotations.SerializedName;

public class OrderAcceptResponse{

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private int status;

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setStatus(int status){
        this.status = status;
    }

    public int getStatus(){
        return status;
    }

    @Override
     public String toString(){
        return 
            "OrderAcceptResponse{" + 
            "message = '" + message + '\'' + 
            ",status = '" + status + '\'' + 
            "}";
        }
}