package com.onedaycart.driver.retrofitModels.orderdetail;

import com.google.gson.annotations.SerializedName;

public class PickupDetails{

	@SerializedName("unit")
	private String unit;

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("product_name")
	private String productName;

	public void setUnit(String unit){
		this.unit = unit;
	}

	public String getUnit(){
		return unit;
	}

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	@Override
 	public String toString(){
		return 
			"PickupDetails{" + 
			"unit = '" + unit + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",product_name = '" + productName + '\'' + 
			"}";
		}
}