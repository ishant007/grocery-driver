package com.onedaycart.driver.retrofitModels.orderdetail;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("order_accepted_status")
	private String orderAcceptedStatus;

	@SerializedName("order_date")
	private String orderDate;

	@SerializedName("delivery_address")
	private String deliveryAddress;

	@SerializedName("picakup_address")
	private String picakupAddress;

	@SerializedName("pickup_details")
	private PickupDetails pickupDetails;

	@SerializedName("contact_detail")
	private String contactDetail;

	public void setOrderAcceptedStatus(String orderAcceptedStatus){
		this.orderAcceptedStatus = orderAcceptedStatus;
	}

	public String getOrderAcceptedStatus(){
		return orderAcceptedStatus;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setDeliveryAddress(String deliveryAddress){
		this.deliveryAddress = deliveryAddress;
	}

	public String getDeliveryAddress(){
		return deliveryAddress;
	}

	public void setPicakupAddress(String picakupAddress){
		this.picakupAddress = picakupAddress;
	}

	public String getPicakupAddress(){
		return picakupAddress;
	}

	public void setPickupDetails(PickupDetails pickupDetails){
		this.pickupDetails = pickupDetails;
	}

	public PickupDetails getPickupDetails(){
		return pickupDetails;
	}

	public void setContactDetail(String contactDetail){
		this.contactDetail = contactDetail;
	}

	public String getContactDetail(){
		return contactDetail;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"order_accepted_status = '" + orderAcceptedStatus + '\'' + 
			",order_date = '" + orderDate + '\'' + 
			",delivery_address = '" + deliveryAddress + '\'' + 
			",picakup_address = '" + picakupAddress + '\'' + 
			",pickup_details = '" + pickupDetails + '\'' + 
			",contact_detail = '" + contactDetail + '\'' + 
			"}";
		}
}