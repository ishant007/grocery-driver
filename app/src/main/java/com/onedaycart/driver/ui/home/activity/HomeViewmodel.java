package com.onedaycart.driver.ui.home.activity;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.onedaycart.driver.baseclasses.BaseViewModel;
import com.onedaycart.driver.prefrences.SharedPre;
import com.onedaycart.driver.retrofitModels.acceptOrder.OrderAcceptResponse;
import com.onedaycart.driver.retrofitModels.orderlistresponse.OrderListResponse;
import com.onedaycart.driver.setting.AppConstance;
import com.onedaycart.driver.ui.home.HomeNav;

import java.util.ArrayList;

public class HomeViewmodel extends BaseViewModel<HomeNav> {
    public HomeViewmodel(Context context, SharedPre sharedPre, Activity activity) {
        super(context, sharedPre, activity);
    }

    public void GetOrderList() {
        getNavigator().showLoading(true);
        AndroidNetworking.post(AppConstance.BASE_URL2 + AppConstance.ORDER_LIST)
                .addBodyParameter("driver_id", getSharedPre().getUserId()) // posting json
                //  .addBodyParameter("driver_id","MI947842211") // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(OrderListResponse.class, new ParsedRequestListener<OrderListResponse>() {
                    @Override
                    public void onResponse(OrderListResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getStatus() == 200 && response.getData() != null && response.getData().size() > 0) {
                            getNavigator().GetOrderList(response.getData());
                        } else {
                            getNavigator().GetOrderList(new ArrayList<>());
                            getNavigator().NoOrderList();
                            getNavigator().showMessage("No Order Found");
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }

    public void getCompleteOrderList() {
        getNavigator().showLoading(true);
        AndroidNetworking.post(AppConstance.BASE_URL2 + AppConstance.ORDER_LIST_COMPLETE)
                .addBodyParameter("driver_id", getSharedPre().getUserId()) // posting json
                //  .addBodyParameter("driver_id","MI947842211") // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(OrderListResponse.class, new ParsedRequestListener<OrderListResponse>() {
                    @Override
                    public void onResponse(OrderListResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getStatus() == 200 && response.getData() != null && response.getData().size() > 0) {
                            getNavigator().GetOrderList(response.getData());
                        } else {
                            getNavigator().GetOrderList(new ArrayList<>());
                            getNavigator().NoOrderList();
                            getNavigator().showMessage("No Order Found");
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }

    public void AcceptOrderNow(String partialOrderId, int position) {
        getNavigator().showLoading(true);
        AndroidNetworking.post(AppConstance.BASE_URL2 + AppConstance.ACCEPT_ORDER)
                .addBodyParameter("order_id", partialOrderId) // posting json
                .addBodyParameter("driver_id", getSharedPre().getUserId()) // posting json
                //  .addBodyParameter("driver_id","MI947842211") // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(OrderAcceptResponse.class, new ParsedRequestListener<OrderAcceptResponse>() {
                    @Override
                    public void onResponse(OrderAcceptResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getStatus() == 200) {
                            getNavigator().AcceptedNow(response.getMessage(),position);
                        } else {
                            getNavigator().showMessage(response.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }
}
