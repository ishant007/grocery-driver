package com.onedaycart.driver.ui.forgetpass;

public interface SetPassNav {
    void showLoading(boolean b);

    void showMessage(String s);

    void getOtp(int otp);
}
