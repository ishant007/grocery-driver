package com.onedaycart.driver.ui.forgetpass;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.onedaycart.driver.R;
import com.onedaycart.driver.baseclasses.BaseActivity;
import com.onedaycart.driver.databinding.ActivitySetPassBinding;
import com.onedaycart.driver.setting.CommonUtils;

import in.aabhasjindal.otptextview.OTPListener;

public class SetPassActivity extends BaseActivity<ActivitySetPassBinding, SetPassViewmodel> implements SetPassNav {
    private String otpMain;
    private CountDownTimer countDownTimer;
    private ActivitySetPassBinding binding;
    private SetPassViewmodel viewmodel;
    private Handler emailHandler = new Handler();
    private Runnable emailRunnable;


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_set_pass;
    }

    @Override
    public SetPassViewmodel getViewModel() {
        return viewmodel = new SetPassViewmodel(this, getSharedPre(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewmodel.setNavigator(this);
        countDownTimer = new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                binding.time.setText("00 : " + millisUntilFinished / 1000);
                binding.resendBtn.setEnabled(false);

            }

            public void onFinish() {
                binding.time.setText("done!");
                binding.resendBtn.setEnabled(true);
            }

        };
        binding.emailOrPhoneTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty() && s.toString().length() >= 10) {
                    emailHandler.removeCallbacksAndMessages(null);
                    emailRunnable = new Runnable() {
                        @Override
                        public void run() {
                            if (!CommonUtils.isValidEmail(s.toString())) {
                                viewmodel.getOtp("email", s.toString());

                            } else if (!CommonUtils.isValidPhoneNumber(s.toString())) {
                                viewmodel.getOtp("phone_no", s.toString());
                            } else {
                                showCustomAlert("Enter Valid Email or Phone number");
                            }
                        }
                    };
                    emailHandler.postDelayed(emailRunnable, 1000);

                } else {
                    emailHandler.removeCallbacksAndMessages(null);
                }

            }
        });
        binding.doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.passTxt == null || binding.passTxt.getText().toString().isEmpty()) {
                    showCustomAlert("Password Not be Empty");
                } else if (binding.confrimTxt == null || binding.confrimTxt.getText().toString().isEmpty()) {
                    showCustomAlert("Confirm Password Not be Empty");
                } else if (!binding.passTxt.getText().toString().trim().equals(binding.confrimTxt.getText().toString().trim())) {
                    showCustomAlert("Password and Confirm Password Not Matched");
                } else {
                    if (!CommonUtils.isValidEmail(binding.emailOrPhoneTxt.getText().toString())) {
                        viewmodel.UpdatePassWord("email", binding.emailOrPhoneTxt.getText().toString(), binding.passTxt.getText().toString().trim());
                    } else {
                        viewmodel.UpdatePassWord("phone_no", binding.emailOrPhoneTxt.getText().toString(), binding.passTxt.getText().toString().trim());
                    }

                }
            }
        });
        binding.otpView.setOtpListener(new OTPListener() {
            @Override
            public void onInteractionListener() {

            }

            @Override
            public void onOTPComplete(String otp) {
                binding.emailOrPhoneTxt.setEnabled(false);
                binding.passTxt.setEnabled(true);
                binding.confrimTxt.setEnabled(true);
                binding.doneBtn.setEnabled(true);
            }
        });

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getInternetDialog().dismiss();

        } else {
            getInternetDialog().show();
        }
    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }

    }

    @Override
    public void showMessage(String s) {
        showCustomAlert(s);
    }

    @Override
    public void getOtp(int otp) {
        countDownTimer.start();
        binding.otpView.setOTP(String.valueOf(otp));
        binding.otpView.requestFocusOTP();
    }
}