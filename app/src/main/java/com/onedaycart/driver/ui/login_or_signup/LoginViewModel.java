package com.onedaycart.driver.ui.login_or_signup;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.onedaycart.driver.baseclasses.BaseViewModel;
import com.onedaycart.driver.prefrences.SharedPre;
import com.onedaycart.driver.retrofitModels.login.LoginResponse;
import com.onedaycart.driver.setting.AppConstance;


public class LoginViewModel extends BaseViewModel<LoginNav> {
    public LoginViewModel(Context context, SharedPre sharedPre, Activity activity) {
        super(context, sharedPre, activity);
    }

    public void LoginNow(String username, String pass,int type) {
        getNavigator().showLoading(true);
        if(type==0){
            AndroidNetworking.post(AppConstance.BASE_URL+AppConstance.LOGIN)
                    .addBodyParameter("email",username) // posting json
                    .addBodyParameter("password",pass) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsObject(LoginResponse.class, new ParsedRequestListener<LoginResponse>() {
                        @Override
                        public void onResponse(LoginResponse response) {
                            getNavigator().showLoading(false);
                            if(response!=null && response.getStatus()==200 ){
                                getSharedPre().setUserId(String.valueOf(response.getData().getDriverId()));
                                getSharedPre().setIsEmailLoggedIn(true);
                                getSharedPre().setUserEmail(response.getData().getEmail());
                                getSharedPre().setName(response.getData().getName());
                                getSharedPre().setEmailProfile(response.getData().getImage());
                                getSharedPre().setUserMobile(response.getData().getPhone());
                                getSharedPre().setRegisterCity(response.getData().getCityId());
                                getNavigator().StartHome();
                            }else{
                                getNavigator().showMessage("Invalid Email or Password !!");
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            getNavigator().showLoading(false);
                            getNavigator().showMessage("Server Not Responding");
                        }
                    });
        }else{
            AndroidNetworking.post(AppConstance.BASE_URL+AppConstance.LOGIN)
                    .addBodyParameter("phone",username) // posting json
                    .addBodyParameter("password",pass) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsObject(LoginResponse.class, new ParsedRequestListener<LoginResponse>() {
                        @Override
                        public void onResponse(LoginResponse response) {
                            getNavigator().showLoading(false);
                            if(response!=null && response.getStatus()==200 ){
                                getSharedPre().setUserId(String.valueOf(response.getData().getDriverId()));
                                getSharedPre().setIsEmailLoggedIn(false);
                                getSharedPre().setUserEmail(response.getData().getEmail());
                                getSharedPre().setName(response.getData().getName());
                                getSharedPre().setEmailProfile(response.getData().getImage());
                                getSharedPre().setUserMobile(response.getData().getPhone());
                                getSharedPre().setRegisterCity(response.getData().getCityId());
                                getNavigator().StartHome();
                            }else{
                                getNavigator().showMessage("Invalid Email or Password !!");
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            getNavigator().showLoading(false);
                            getNavigator().showMessage("Server Not Responding");
                        }
                    });
        }

    }
}
