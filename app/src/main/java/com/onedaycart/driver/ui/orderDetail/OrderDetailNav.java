package com.onedaycart.driver.ui.orderDetail;

import com.onedaycart.driver.retrofitModels.orderdetail.OrderDetailResponse;

public interface OrderDetailNav {
    void showLoading(boolean b);


    void NoOrderDetail();

    void GetOrderDetails(OrderDetailResponse response);

    void showMessage(String no_order_found);
}
