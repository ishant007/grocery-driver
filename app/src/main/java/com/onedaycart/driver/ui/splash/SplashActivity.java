package com.onedaycart.driver.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.onedaycart.driver.BuildConfig;
import com.onedaycart.driver.R;
import com.onedaycart.driver.baseclasses.BaseActivity;
import com.onedaycart.driver.databinding.ActivitySplashBinding;
import com.onedaycart.driver.ui.home.activity.HomeActivity;
import com.onedaycart.driver.ui.login_or_signup.LoginWithPasswordActivity;

import org.json.JSONObject;

import static com.onedaycart.driver.setting.AppConstance.BASE_URL4;
import static com.onedaycart.driver.setting.AppConstance.USER_TOKEN;

public class SplashActivity extends BaseActivity<ActivitySplashBinding,SplashViewModel> {
    private SplashViewModel viewModel;
    private ActivitySplashBinding binding;
    private Handler spalshHandler;
    private Runnable splashRunnable;
private FirebaseAnalytics firebaseAnalytics;
    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        return viewModel==null?new SplashViewModel(this,getSharedPre(),this):viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       binding=getViewDataBinding();
       if(spalshHandler==null){
           spalshHandler=new Handler();
       }
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, BuildConfig.APPLICATION_ID);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        FirebaseMessaging.getInstance().getToken().addOnSuccessListener(new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String newToken) {
                getSharedPre().setFirebaseToken(newToken);
                if (getSharedPre().getFirebaseDeviceToken() != null && !getSharedPre().getFirebaseDeviceToken().isEmpty()) {
                        AndroidNetworking.post(BASE_URL4 + USER_TOKEN)
                                .addBodyParameter("driver_id", getSharedPre().getUserId()) // posting java object
                                .addBodyParameter("device_token", getSharedPre().getFirebaseDeviceToken()) // posting java object
                                .setTag("suplier_token")
                                .setPriority(Priority.HIGH)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if(response!=null){

                                            splashRunnable=new Runnable() {
                                                @Override
                                                public void run() {
                                                    if(getSharedPre().isLoggedIn()) {
                                                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                                        finish();
                                                    }else{
                                                        startActivity(new Intent(SplashActivity.this, LoginWithPasswordActivity.class));
                                                        finish();
                                                    }

                                                }
                                            };
                                            spalshHandler.postDelayed(splashRunnable,1000);

                                        }else{
                                            splashRunnable=new Runnable() {
                                                @Override
                                                public void run() {
                                                    if(getSharedPre().isLoggedIn()) {
                                                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                                        finish();
                                                    }else{
                                                        startActivity(new Intent(SplashActivity.this, LoginWithPasswordActivity.class));
                                                        finish();
                                                    }

                                                }
                                            };
                                            spalshHandler.postDelayed(splashRunnable,1000);

                                        }

                                    }

                                    @Override
                                    public void onError(ANError anError) {
                                        Toast.makeText(SplashActivity.this, "Token Not Updated", Toast.LENGTH_SHORT).show();
                                        splashRunnable=new Runnable() {
                                            @Override
                                            public void run() {
                                                if(getSharedPre().isLoggedIn()) {
                                                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                                    finish();
                                                }else{
                                                    startActivity(new Intent(SplashActivity.this, LoginWithPasswordActivity.class));
                                                    finish();
                                                }

                                            }
                                        };
                                        spalshHandler.postDelayed(splashRunnable,1000);

                                    }
                                });
                } else {
                    splashRunnable=new Runnable() {
                        @Override
                        public void run() {
                            if(getSharedPre().isLoggedIn()) {
                                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                finish();
                            }else{
                                startActivity(new Intent(SplashActivity.this, LoginWithPasswordActivity.class));
                                finish();
                            }

                        }
                    };
                    spalshHandler.postDelayed(splashRunnable,1000);
                }
            }
        });




    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getInternetDialog().dismiss();

        }else{
            getInternetDialog().show();
        }

    }
}