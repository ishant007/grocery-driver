package com.onedaycart.driver.ui.home;

import com.onedaycart.driver.retrofitModels.orderlistresponse.DataItem;

import java.util.List;

public interface HomeNav {
    void showLoading(boolean b);

    void showMessage(String s);

    void GetOrderList(List<DataItem> data);

    void NoOrderList();

    void AcceptedNow(String message, int position);
}
