package com.onedaycart.driver.ui.home.activity;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;

import com.onedaycart.driver.R;
import com.onedaycart.driver.baseclasses.BaseActivity;
import com.onedaycart.driver.databinding.ActivityHomeBinding;
import com.onedaycart.driver.retrofitModels.orderlistresponse.DataItem;
import com.onedaycart.driver.ui.home.HomeNav;
import com.onedaycart.driver.ui.home.PickupAdapter;
import com.onedaycart.driver.ui.orderDetail.OrderDetailActivity;
import com.onedaycart.driver.ui.splash.SplashActivity;

import java.util.List;

public class HomeActivity extends BaseActivity<ActivityHomeBinding, HomeViewmodel> implements HomeNav, PickupAdapter.CallApis {
    private ActivityHomeBinding binding;
    private HomeViewmodel viewmodel;
    private PickupAdapter adapter;
    private Context context;
    private boolean isDelivery = false;

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    public HomeViewmodel getViewModel() {
        return viewmodel = new HomeViewmodel(this, getSharedPre(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewmodel.setNavigator(this);
        context=this;
        adapter = new PickupAdapter(this, this);
        binding.toolbar.header.setText("Orders");
        binding.pickupRecycler.setAdapter(adapter);
        viewmodel.GetOrderList();
        binding.toolbar.logout.setVisibility(View.VISIBLE);
        binding.toolbar.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPre().Logout();
                startActivity(new Intent(context, SplashActivity.class));
                finish();
            }
        });
        binding.pickupLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                binding.deliveryLay.setBackgroundResource(R.drawable.tab_unselected);
                binding.deliveryTxt.setTextColor(getResources().getColor(R.color.gray_txt2));
                binding.pickupLay.setBackgroundResource(R.drawable.tab_selected);
                binding.pickup.setTextColor(getResources().getColor(R.color.saffron));
                binding.pickup.setTypeface(ResourcesCompat.getFont(context, R.font.roboto), Typeface.BOLD);
                binding.deliveryTxt.setTypeface(ResourcesCompat.getFont(context, R.font.montserrat_regular), Typeface.NORMAL);
                viewmodel.GetOrderList();
                isDelivery=false;
            }
        });

        binding.deliveryLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.pickupLay.setBackgroundResource(R.drawable.tab_unselected);
                binding.pickup.setTextColor(getResources().getColor(R.color.gray_txt2));
                binding.deliveryLay.setBackgroundResource(R.drawable.tab_selected);
                binding.deliveryTxt.setTextColor(getResources().getColor(R.color.saffron));
                binding.deliveryTxt.setTypeface(ResourcesCompat.getFont(context, R.font.roboto), Typeface.BOLD);
                binding.pickup.setTypeface(ResourcesCompat.getFont(context, R.font.montserrat_regular), Typeface.NORMAL);
               viewmodel.getCompleteOrderList();
               isDelivery=true;
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getInternetDialog().dismiss();

        } else {
            getInternetDialog().show();
        }

    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void showMessage(String s) {
        showCustomAlert(s);
    }

    @Override
    public void GetOrderList(List<DataItem> data) {
        adapter.UpdaetList(data);
        binding.emptyTxt.setVisibility(View.GONE);
    }

    @Override
    public void NoOrderList() {
        binding.emptyTxt.setVisibility(View.VISIBLE);
    }

    @Override
    public void AcceptedNow(String s,int pos) {
        viewmodel.GetOrderList();
        showCustomAlert(s);
    }

    @Override
    public void AcceptOrderRequest(String partialOrderId, int position) {
        viewmodel.AcceptOrderNow(partialOrderId,position);
    }

    @Override
    public void callViewMoreAction(String status, String pOrderId) {
   startActivityForResult(new Intent(context, OrderDetailActivity.class).putExtra("status",status).putExtra("pOrderId", pOrderId),100);
    }

    @Override
    public void customMessage(String message) {
        showMessage(message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100 && resultCode==RESULT_OK){
            if(isDelivery){
                viewmodel.getCompleteOrderList();
            }else{
                viewmodel.GetOrderList();
            }

        }
    }
}