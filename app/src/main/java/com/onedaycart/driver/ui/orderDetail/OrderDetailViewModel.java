package com.onedaycart.driver.ui.orderDetail;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.onedaycart.driver.baseclasses.BaseViewModel;
import com.onedaycart.driver.prefrences.SharedPre;
import com.onedaycart.driver.retrofitModels.orderdetail.OrderDetailResponse;
import com.onedaycart.driver.retrofitModels.updatestatus.UpdateStatusResponse;
import com.onedaycart.driver.setting.AppConstance;

public class OrderDetailViewModel extends BaseViewModel<OrderDetailNav> {
    public OrderDetailViewModel(Context context, SharedPre sharedPre, Activity activity) {
        super(context, sharedPre, activity);
    }
    public void GetOrderDetail(String partialId) {
        getNavigator().showLoading(true);
        AndroidNetworking.post(AppConstance.BASE_URL2 + AppConstance.ORDER_DETAIL)
                .addBodyParameter("partial_order_id", partialId) // posting json
                .addBodyParameter("driver_id", getSharedPre().getUserId()) // posting json
                //  .addBodyParameter("driver_id","MI947842211") // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(OrderDetailResponse.class, new ParsedRequestListener<OrderDetailResponse>() {
                    @Override
                    public void onResponse(OrderDetailResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getStatus() == 200 ) {
                            getNavigator().GetOrderDetails(response);
                        } else {
                            getNavigator().NoOrderDetail();
                            getNavigator().showMessage("No Order Found");
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }
    public void UpdateOrderStatus(String partialId,String orderStatus) {
        getNavigator().showLoading(true);
        AndroidNetworking.post(AppConstance.BASE_URL2 + AppConstance.ORDER_STATUS)
                .addBodyParameter("partial_order_id", partialId) // posting json
                .addBodyParameter("driver_id", getSharedPre().getUserId()) // posting json
                .addBodyParameter("internal_order_status", orderStatus) // posting json
                .addBodyParameter("customer_order_status", orderStatus) // posting json
                //  .addBodyParameter("driver_id","MI947842211") // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(UpdateStatusResponse.class, new ParsedRequestListener<UpdateStatusResponse>() {
                    @Override
                    public void onResponse(UpdateStatusResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getStatus() == 200 ) {
                            GetOrderDetail(partialId);
                            getNavigator().showMessage(response.getMessage());
                        } else {
                            getNavigator().showMessage(response.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }
}
