package com.onedaycart.driver.ui.forgetpass;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.onedaycart.driver.baseclasses.BaseViewModel;
import com.onedaycart.driver.prefrences.SharedPre;
import com.onedaycart.driver.retrofitModels.otpres.OtpResponse;
import com.onedaycart.driver.retrofitModels.passreset.ResetResponse;
import com.onedaycart.driver.setting.AppConstance;

public class SetPassViewmodel extends BaseViewModel<SetPassNav> {
    public SetPassViewmodel(Context context, SharedPre sharedPre, Activity activity) {
        super(context, sharedPre, activity);
    }

    public void getOtp(String type,String emailOrPhone) {
        getNavigator().showLoading(true);
            AndroidNetworking.post(AppConstance.BASE_URL3+AppConstance.SEND_OTP)
                    .addBodyParameter("type",emailOrPhone) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsObject(OtpResponse.class, new ParsedRequestListener<OtpResponse>() {
                        @Override
                        public void onResponse(OtpResponse response) {
                            getNavigator().showLoading(false);
                            if(response!=null && response.getStatus()==200 ){
                               getNavigator().getOtp(response.getOtp());
                            }else{
                                getNavigator().showMessage("Invalid Email or Password !!");
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            getNavigator().showLoading(false);
                            getNavigator().showMessage("Server Not Responding");
                        }
                    });
    }

    public void UpdatePassWord(String type,String emailOrPhone,String password) {
            getNavigator().showLoading(true);

                AndroidNetworking.post(AppConstance.BASE_URL3+AppConstance.RESET)
                        .addBodyParameter("type",emailOrPhone) // posting json
                        .addBodyParameter("password",password) // posting json
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsObject(ResetResponse.class, new ParsedRequestListener<ResetResponse>() {
                            @Override
                            public void onResponse(ResetResponse response) {
                                getNavigator().showLoading(false);
                                if(response!=null && response.getStatus()==200 ){
                                    getNavigator().showMessage("Password Reset Successfully !!");
                                }else{
                                    getNavigator().showMessage("Invalid Email or Password !!");
                                }

                            }

                            @Override
                            public void onError(ANError anError) {
                                getNavigator().showLoading(false);
                                getNavigator().showMessage("Server Not Responding");
                            }
                        });
    }
}
