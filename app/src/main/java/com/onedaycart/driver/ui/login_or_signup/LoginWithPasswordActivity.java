package com.onedaycart.driver.ui.login_or_signup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.InputType;
import android.view.View;

import com.onedaycart.driver.R;
import com.onedaycart.driver.baseclasses.BaseActivity;
import com.onedaycart.driver.databinding.ActivityLoginWithPasswordBinding;
import com.onedaycart.driver.setting.CommonUtils;
import com.onedaycart.driver.ui.forgetpass.SetPassActivity;
import com.onedaycart.driver.ui.home.activity.HomeActivity;

public class LoginWithPasswordActivity extends BaseActivity<ActivityLoginWithPasswordBinding, LoginViewModel> implements LoginNav {
    private ActivityLoginWithPasswordBinding binding;
    private LoginViewModel viewmodel;
    private Activity activity;


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login_with_password;
    }

    @Override
    public LoginViewModel getViewModel() {
        return viewmodel = new LoginViewModel(this, getSharedPre(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewmodel.setNavigator(this);
        activity=this;
        binding.toolbar.back.setVisibility(View.VISIBLE);
        binding.toolbar.logout.setVisibility(View.GONE);
        binding.toolbar.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        binding.toolbar.header.setText(R.string.login_with_pass);
        binding.forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, SetPassActivity.class));
            }
        });
        binding.password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.password.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    binding.password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    binding.password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.visiblity_ico, 0);
                } else {
                    binding.password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.not_visiblity_ico, 0);
                    binding.password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }
            }
        });
        binding.loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.emailOrPhoneTxt.getText().toString() == null || binding.emailOrPhoneTxt.getText().toString().isEmpty()) {
                    showCustomAlert("Please Enter Email or Phone");
                } else if (binding.password.getText().toString() == null || binding.password.getText().toString().isEmpty()) {
                    showCustomAlert("Please Enter Password");
                    binding.errorMessage.setVisibility(View.VISIBLE);
                } else if (!CommonUtils.isValidEmail(binding.emailOrPhoneTxt.getText().toString()) && !binding.password.getText().toString().isEmpty()) {
                    binding.errorMessage.setVisibility(View.INVISIBLE);
                    viewmodel.LoginNow(binding.emailOrPhoneTxt.getText().toString(), binding.password.getText().toString(), 0);
                } else if (!PhoneNumberUtils.isGlobalPhoneNumber(binding.emailOrPhoneTxt.getText().toString()) && !binding.password.getText().toString().isEmpty()) {
                    viewmodel.LoginNow(binding.emailOrPhoneTxt.getText().toString(), binding.password.getText().toString(), 1);
                    binding.errorMessage.setVisibility(View.INVISIBLE);
                }
            }
        });


    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getInternetDialog().dismiss();
        } else {
            getInternetDialog().show();
        }
    }

    @Override
    public void StartHome() {
        getSharedPre().setIsLoggedIn(true);
        startActivity(new Intent(LoginWithPasswordActivity.this, HomeActivity.class));
        finish();
    }

    @Override
    public void showLoading(boolean b) {
        if(b){
            showLoadingDialog("");
        }else{
            hideLoadingDialog();
        }
    }

    @Override
    public void showMessage(String s) {
        showCustomAlert(s);
    }
}