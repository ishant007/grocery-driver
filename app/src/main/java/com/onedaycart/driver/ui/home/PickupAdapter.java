package com.onedaycart.driver.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.onedaycart.driver.R;
import com.onedaycart.driver.databinding.PickupItemBinding;
import com.onedaycart.driver.retrofitModels.orderlistresponse.DataItem;

import java.util.ArrayList;
import java.util.List;

public class PickupAdapter extends RecyclerView.Adapter<PickupAdapter.PickupViewHolder> {
    private Context context;
    private List<DataItem> orderLIst = new ArrayList<>();
    private CallApis callApis;

    public PickupAdapter(Context context, CallApis callApis) {
        this.context = context;
        this.callApis = callApis;

    }

    @NonNull
    @Override
    public PickupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PickupItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.pickup_item, parent, false);
        return new PickupViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PickupViewHolder holder, int position) {
        holder.onViewBind(orderLIst.get(position), position);
    }

    @Override
    public int getItemCount() {
        return orderLIst.size();
    }


    public void UpdaetList(List<DataItem> data) {
        orderLIst = data;
        notifyDataSetChanged();
    }

    class PickupViewHolder extends RecyclerView.ViewHolder {
        private PickupItemBinding binding;

        public PickupViewHolder(@NonNull PickupItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void onViewBind(DataItem dataItem, int position) {
            binding.orderId.setText("Order Id : " + dataItem.getOrderId());
            binding.porderId.setText("Partial Id : " + dataItem.getPartialOrderId());
            binding.addressPickup.setText(dataItem.getAddress());
            if (dataItem.getOrderAcceptedStatus().equalsIgnoreCase("active")) {
                if (dataItem.getStatus().equalsIgnoreCase("completed")) {
                    binding.status.setBackgroundResource(R.drawable.grenn_outline_button);
                    binding.status.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_green, 0);
                    binding.status.setTextColor(context.getResources().getColor(R.color.green));
                    binding.status.setText(dataItem.getStatus());
                } else {
                    binding.status.setBackgroundResource(R.drawable.outline_btn);
                    binding.status.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pendind_ico, 0);
                    binding.status.setTextColor(context.getResources().getColor(R.color.saffron));
                    binding.status.setText(dataItem.getStatus());
                }
                binding.status.setOnClickListener(null);
            } else {
                if (dataItem.getStatus().equalsIgnoreCase("completed")) {
                    binding.status.setBackgroundResource(R.drawable.grenn_outline_button);
                    binding.status.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_green, 0);
                    binding.status.setTextColor(context.getResources().getColor(R.color.green));
                    binding.status.setText(dataItem.getStatus());
                } else {
                    binding.status.setBackgroundResource(R.drawable.red_accept_back);
                    binding.status.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pendind_ico, 0);
                    binding.status.setTextColor(context.getResources().getColor(R.color.white));
                    binding.status.setText("Accept");
                }
                binding.status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callApis.AcceptOrderRequest(dataItem.getPartialOrderId(), position);
                    }
                });
            }
            binding.viewMoreBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(binding.status.getText().toString().equalsIgnoreCase("Accept")){
                       callApis.customMessage("Please Accept the Order ");
                    }else{
                        callApis.callViewMoreAction(dataItem.getStatus(),dataItem.getPartialOrderId());
                    }

                    }
            });
        }
    }

    public interface CallApis {
        void AcceptOrderRequest(String partialOrderId, int position);
        void callViewMoreAction(String status,String pOrderId);
        void customMessage(String message);
    }
}
