package com.onedaycart.driver.ui.orderDetail;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.onedaycart.driver.R;
import com.onedaycart.driver.databinding.PickupDetailItemBinding;
import com.onedaycart.driver.retrofitModels.orderdetail.PickupDetails;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.OrderDetailViewholder> {
  private List<PickupDetails>getAllOrdersDetails=new ArrayList<>();
    @NonNull
    @Override
    public OrderDetailViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PickupDetailItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.pickup_detail_item, parent, false);
        return new OrderDetailViewholder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDetailViewholder holder, int position) {
        holder.onViewBindinf(getAllOrdersDetails.get(position),position);
    }

    @Override
    public int getItemCount() {
        return getAllOrdersDetails.size();
    }

    public void UpdateList(PickupDetails pickupDetails) {
        getAllOrdersDetails=new ArrayList<>();
        getAllOrdersDetails.add(pickupDetails);
        notifyDataSetChanged();
    }

    class OrderDetailViewholder extends RecyclerView.ViewHolder {
        private PickupDetailItemBinding binding;

        public OrderDetailViewholder(@NonNull PickupDetailItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void onViewBindinf(PickupDetails pickupDetails, int position) {
            binding.productName.setText(pickupDetails.getProductName());
            binding.unite.setText(pickupDetails.getUnit());
            binding.qty.setText(String.valueOf(pickupDetails.getQuantity()));
        }
    }
}
