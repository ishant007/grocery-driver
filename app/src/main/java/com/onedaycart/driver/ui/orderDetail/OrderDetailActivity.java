
package com.onedaycart.driver.ui.orderDetail;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.jakewharton.rxbinding3.view.RxView;
import com.onedaycart.driver.R;
import com.onedaycart.driver.baseclasses.BaseActivity;
import com.onedaycart.driver.databinding.ActivityOrderDetailBinding;
import com.onedaycart.driver.retrofitModels.orderdetail.OrderDetailResponse;
import com.onedaycart.driver.setting.CommonUtils;
import com.onedaycart.driver.ui.splash.SplashActivity;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class OrderDetailActivity extends BaseActivity<ActivityOrderDetailBinding, OrderDetailViewModel> implements OrderDetailNav {
    private ActivityOrderDetailBinding binding;
    private OrderDetailViewModel viewModel;
    private OrderDetailAdapter adapter;
    private String[] pickup_status = {"Select","On-Way", "Completed"};
    private ArrayAdapter spinnerPickupAdapter;
    private String partialId, status;
    private Disposable disposable;
    private boolean isApiCalled=false;

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_order_detail;
    }

    @Override
    public OrderDetailViewModel getViewModel() {
        return viewModel = new OrderDetailViewModel(this, getSharedPre(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        partialId = getIntent().getStringExtra("pOrderId");
        status = getIntent().getStringExtra("status");
        viewModel.setNavigator(this);
        adapter = new OrderDetailAdapter();
        viewModel.GetOrderDetail(partialId);
        spinnerPickupAdapter = new ArrayAdapter(this, R.layout.custom_spinner, pickup_status);
        binding.pickupStatusSpinner.setAdapter(spinnerPickupAdapter);
        binding.toolbar.header.setText("Order Details");
        binding.toolbar.header.setVisibility(View.VISIBLE);
        binding.pickupDetailRecycler.setAdapter(adapter);
        binding.toolbar.logout.setVisibility(View.VISIBLE);
        binding.toolbar.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPre().Logout();
                startActivity(new Intent(OrderDetailActivity.this, SplashActivity.class));
                finish();
            }
        });
        binding.pickupStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binding.pickupStatus.setText(pickup_status[position]);
                if (position > 0) {
                    status = pickup_status[position];
                    viewModel.UpdateOrderStatus(partialId, pickup_status[position]);
                    isApiCalled=true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                binding.pickupStatus.setText("Pickup Status");

            }
        });
        binding.backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getInternetDialog().dismiss();

        } else {
            getInternetDialog().show();
        }

    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void NoOrderDetail() {
        onBackPressed();
    }

    @Override
    public void GetOrderDetails(OrderDetailResponse response) {
        if (response.getData() != null) {
            binding.pickupStatus.setText(status);
            adapter.UpdateList(response.getData().getPickupDetails());
            binding.addressPickup.setText(response.getData().getPicakupAddress());
            binding.addressDeleivery.setText(response.getData().getDeliveryAddress());
            binding.phoneNumberTxt.setText(response.getData().getContactDetail());
            if (response.getData().getOrderAcceptedStatus().equalsIgnoreCase(getString(R.string.order_accept)) && !status.equalsIgnoreCase(getString(R.string.completed))) {
                binding.hideContactView.setVisibility(View.GONE);
                disposable = RxView
                        .clicks(binding.callNowBtn)
                        .observeOn(AndroidSchedulers.mainThread())
                        .throttleFirst(1000, TimeUnit.MILLISECONDS)
                        .compose(getRxPermissions().ensure(Manifest.permission.CALL_PHONE))
                        .subscribe(aBoolean -> {
                    if (aBoolean) {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + response.getData().getContactDetail()));
                        startActivity(intent);
                    }
                });
                binding.pickupStatus.setOnClickListener(v -> binding.pickupStatusSpinner.performClick());
            } else if (response.getData().getOrderAcceptedStatus().equalsIgnoreCase(getString(R.string.order_accept)) && status.equalsIgnoreCase(getString(R.string.completed))) {
                binding.pickupStatus.setOnClickListener(null);
                binding.hideContactView.setVisibility(View.VISIBLE);
                binding.hideContactView.setText(R.string.order_completed);
                binding.callNowBtn.setOnClickListener(null);
            } else {
                binding.pickupStatus.setOnClickListener(null);
                binding.hideContactView.setVisibility(View.VISIBLE);
                binding.hideContactView.setText(R.string.not_accepted_yet);
                binding.callNowBtn.setOnClickListener(null);
            }
            binding.dateOrder.setText(CommonUtils.getFormattedDate(response.getData().getOrderDate(), "yyyy-MM-dd", "dd,MMM yyyy"));
            binding.orderId.setText("OrderId : " + partialId);
            binding.totalItem.setText("1 Item");
        }
    }

    @Override
    public void showMessage(String no_order_found) {
        showCustomAlert(no_order_found);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposable != null) {
            disposable.dispose();
        }
    }

    @Override
    public void onBackPressed() {
        if(isApiCalled){
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }else{
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
    }
}