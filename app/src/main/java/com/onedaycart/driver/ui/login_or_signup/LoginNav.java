package com.onedaycart.driver.ui.login_or_signup;

public interface LoginNav {
    void StartHome();

    void showLoading(boolean b);

    void showMessage(String s);
}
