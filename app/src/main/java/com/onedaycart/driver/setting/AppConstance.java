package com.onedaycart.driver.setting;

public class AppConstance {

    public static final String LOGIN_TYPE_EMAIL = "email";
    public static final String LOGIN_TYPE_FACEBOOK = "facebook";
    public static final String LOGIN_TYPE_GOOGLE = "google";
    public static final String DEVICE_TYPE = "android";
    //API
   /* public static final String BASE_URL = "https://kamakhyaits.com/grocery_user/public/driver/";
    public static final String BASE_URL2 = "https://kamakhyaits.com/grocery_order/public/Api/driver/";
    public static final String BASE_URL3 = "https://kamakhyaits.com/grocery_user/public/api/";*/
    public static final String BASE_URL = "https://lvlvv2xqj8.execute-api.ap-south-1.amazonaws.com/user/public/driver/";
    public static final String BASE_URL2 = "https://lvlvv2xqj8.execute-api.ap-south-1.amazonaws.com/order/public/Api/driver/";
    public static final String BASE_URL3 = "https://lvlvv2xqj8.execute-api.ap-south-1.amazonaws.com/user/public/api/";
    public static final String BASE_URL4 = "https://lvlvv2xqj8.execute-api.ap-south-1.amazonaws.com/user/";
    public static final String LOGIN = "login";
    public static final String ORDER_LIST = "order/pending";
    public static final String ORDER_LIST_COMPLETE= "order/completed";
    public static final String ACCEPT_ORDER = "order/orderaccept";
    public static final String ORDER_DETAIL = "order/detail";
    public static final String ORDER_STATUS = "order/status";
    public static final String RESET = "password/reset";
    public static final String SEND_OTP = "driver/password/otp";
    public static String USER_TOKEN="api/update-device-token";

}

