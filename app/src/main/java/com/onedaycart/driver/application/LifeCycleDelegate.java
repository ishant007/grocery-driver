/*
 * Copyright (c) Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 * 7732993378
 */
package com.onedaycart.driver.application;

public interface LifeCycleDelegate {
    void onAppBackgrounded();

    void onAppForegrounded();
}
