package com.onedaycart.driver.fcm;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.onedaycart.driver.prefrences.SharedPre;
import com.onedaycart.driver.R;
import com.onedaycart.driver.ui.home.activity.HomeActivity;
import org.json.JSONObject;
import java.io.IOException;
import java.util.Map;
import static com.onedaycart.driver.setting.AppConstance.BASE_URL4;
import static com.onedaycart.driver.setting.AppConstance.USER_TOKEN;


public class NotificationManager extends FirebaseMessagingService {
    private SharedPre sharedPre;
    private boolean isMuted=false,SendNotification=false;
    private Uri uri;
    private String title,type,body;
    private Intent intent;
    private String token;
    private static final int NOTIFICATION_ID = 3;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        token=s;
        if(sharedPre==null){
            sharedPre= SharedPre.getInstance(this);
        }
        sharedPre.setFirebaseToken(s);
        sendRegistrationToServer(s);

    }

    private void sendRegistrationToServer(String token) {
        if(sharedPre==null){
            sharedPre= SharedPre.getInstance(this);
        }
        AndroidNetworking.post(BASE_URL4 + USER_TOKEN)
                .addBodyParameter("driver_id", sharedPre.getUserId()) // posting java object
                .addBodyParameter("device_token", token) // posting java object
                .setTag("suplier_token")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null){

                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        if(sharedPre==null){
            sharedPre= SharedPre.getInstance(this);
        }
        isMuted = sharedPre.isNotificationMuted();
        if (remoteMessage != null) {
            try {
                if (isMuted) {
                    uri = null;
                } else {
                    String sound = sharedPre.getNotificationSound();
                    if (sound == null || sound.equals("")) {
                        uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    } else {
                        uri = Uri.parse(sharedPre.getNotificationSound());
                    }
                }
            } catch (Exception e) {

            }
            super.onMessageReceived(remoteMessage);
                try {
                    SendNotification=sharedPre.isLoggedIn();
                    title = getString(R.string.app_name);
                    Map<String, String> map = remoteMessage.getData();
                    handleDataMessage(map);

                } catch (Exception e) {
                    Intent intent = new Intent(this, HomeActivity.class);
                    showNotification(title, body, intent);
                }
            }



    }
    private void handleDataMessage(Map<String, String> json){
        Log.e(getString(R.string.app_name), "push json: " + json.toString());

        try {
            if (json != null) {
                String title = json.get("title");
                String message = json.get("body");

                if (SendNotification) {
                    intent=new Intent(this, HomeActivity.class);
                    showNotification(title, message, intent);
                }

            }
        } catch (Exception e) {
            Log.e(getString(R.string.app_name), "Exception: " + e.getMessage());
            if (SendNotification) {
                intent=new Intent(this, HomeActivity.class);
                showNotification(getString(R.string.app_name), "New Notification ", intent);
            }

        }
    }
    public void showNotification(String title, String body, Intent intent) {
        Bitmap logo;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(HomeActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT);
        // PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //to be able to launch your activity from the notification
        @SuppressLint("WrongConstant")
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, getString(R.string.app_name));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.mipmap.ic_launcher);
            builder.setColor(getResources().getColor(R.color.transparent));
        } else {
            builder.setSmallIcon(R.mipmap.ic_launcher);
        }
        logo = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        builder.setContentTitle(title)
                .setContentText(body)
                .setLargeIcon(logo)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setColorized(true)
                .setAutoCancel(true)
                .setSound(null)
                .setNotificationSilent()
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setDefaults(Notification.BADGE_ICON_LARGE)
                .setLights(1, 1, 1)
                .setOngoing(false)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = getString(R.string.app_name);
            int importance = android.app.NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(getString(R.string.app_name), name, importance);
            channel.setDescription(description);
            channel.enableVibration(true);
            channel.setSound(null, null);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            android.app.NotificationManager notificationManager = getSystemService(android.app.NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        } else {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            Notification notificationCompat = builder.build();
            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);
            managerCompat.notify(NOTIFICATION_ID, notificationCompat);
        }


        if (!sharedPre.isNotificationMuted()) {
            playSound(this);
        }


    }

    public void playSound(Context context) {
        AudioManager myAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int i = myAudioManager.getRingerMode();

        if (myAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE)
            uri = Uri.parse(sharedPre.getNotificationSound());

        if (sharedPre.getNotificationSound() == null || sharedPre.getNotificationSound().equals("")) {
            uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }

        MediaPlayer mediaPlayer = new MediaPlayer();

        try {
            mediaPlayer.setDataSource(context, uri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            mediaPlayer.start();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
